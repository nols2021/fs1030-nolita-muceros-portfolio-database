-- MySQL dump 10.13  Distrib 8.0.27, for macos11 (x86_64)
--
-- Host: localhost    Database: mywebsite
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `highlight_detail`
--

DROP TABLE IF EXISTS `highlight_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `highlight_detail` (
  `highlight_detail_id` int unsigned NOT NULL AUTO_INCREMENT,
  `display_order` int unsigned NOT NULL,
  `detail` varchar(255) NOT NULL,
  `highlight_id` int unsigned NOT NULL,
  PRIMARY KEY (`highlight_detail_id`,`display_order`),
  KEY `highlight_id` (`highlight_id`),
  CONSTRAINT `highlight_detail_ibfk_1` FOREIGN KEY (`highlight_id`) REFERENCES `highlight` (`highlight_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `highlight_detail`
--

LOCK TABLES `highlight_detail` WRITE;
/*!40000 ALTER TABLE `highlight_detail` DISABLE KEYS */;
INSERT INTO `highlight_detail` VALUES (1,1,'Front-end: HTML5, CSS3, Bootstrap, JavaScript (ES6+), JSX, HTML, DOM, React, AJAX',1),(2,2,'Back-end: Node.js, Express.js, REST, Restful Routes',1),(3,3,'Database: MySQL, mySQL Workbench, PHP, MongoDB',1),(4,4,'Project Management Tools: Trello, Gitlab, Github, Darw.io',1),(5,5,'Other: Promises, JASON, DOM API, Restful API, Fetch API,  Docker, GCP (Google Cloud Platform), AWS (Amazon Web Services), Git',1);
/*!40000 ALTER TABLE `highlight_detail` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-12  9:33:09
