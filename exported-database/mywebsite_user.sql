-- MySQL dump 10.13  Distrib 8.0.27, for macos11 (x86_64)
--
-- Host: localhost    Database: mywebsite
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_first_name` varchar(40) NOT NULL,
  `user_last_name` varchar(80) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(80) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','user','$argon2i$v=19$m=4096,t=3,p=1$c9WDPK6i4Va55ySDpzy+ZQ$NsEW2PRRoQpQAeBVRjBCifBndEnLv4jHDJHt9AJKQDw','adminuser@yehey.ca'),(2,'admin2','user2','$argon2i$v=19$m=4096,t=3,p=1$sotBHeLLeuDxFn4YMKAeIg$DW37iMQ/3q4zQVcsL2S/++rX7dHtRmikZigYpNVP2wY','sample@user.com'),(3,'User1','One','$argon2i$v=19$m=4096,t=3,p=1$8Ajg3zvjku14NnGdur+8NQ$bLt6kmmf2S9W5IXTTRdm79mS2XC8EvfnwXqSskhm+FU','user1@gmail.com'),(4,'User2','Two','$argon2i$v=19$m=4096,t=3,p=1$1v7pXPM4+NQxLzFwoLQaJQ$rSEBdx2YXc2FW0sp3AiDWhwvEPWRV67LEd2XBBL1Xqk','user2@yehey.ca'),(5,'User3','Three','$argon2i$v=19$m=4096,t=3,p=1$zA+jPvLQO63rimgbgJvNjg$3/xGtJXZBBi4d35ui8lnMJNjt6x4llsumn4XxhNBbVI','user3@yehey.com'),(6,'User4','Four','$argon2i$v=19$m=4096,t=3,p=1$FdsPbyR6excqxleuyLrtkA$wFIxl97gyuQiqHECzffwWWVZfRAVeYvUTYlXs8aoUcA','user4@hotmail.com'),(7,'User5','Five','$argon2i$v=19$m=4096,t=3,p=1$GYeI+IKq8MfOf6dzy12YKg$ZAl7Dfepvtkoejw38fhFmaRoZRUWiolzXtlK2cvtyxo','user5@bell.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-12  9:33:10
