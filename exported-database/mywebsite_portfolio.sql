-- MySQL dump 10.13  Distrib 8.0.27, for macos11 (x86_64)
--
-- Host: localhost    Database: mywebsite
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `portfolio`
--

DROP TABLE IF EXISTS `portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portfolio` (
  `portfolio_id` int unsigned NOT NULL AUTO_INCREMENT,
  `display_order` int unsigned NOT NULL,
  `title` varchar(80) NOT NULL,
  `sub_title` varchar(80) NOT NULL,
  `details` varchar(3000) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `resume_id` int unsigned NOT NULL,
  PRIMARY KEY (`portfolio_id`),
  UNIQUE KEY `display_order` (`display_order`),
  KEY `resume_id` (`resume_id`),
  CONSTRAINT `portfolio_ibfk_1` FOREIGN KEY (`resume_id`) REFERENCES `resume` (`resume_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio`
--

LOCK TABLES `portfolio` WRITE;
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;
INSERT INTO `portfolio` VALUES (1,1,'YOH','YorkU Hackathon 2021 Winner!','Winner of the York University Hackathon 2021. YOH! is a hub where tech graduates from York University are able to connect with recruiters for potential job opportunities. I worked mainly on the front-end.','https://devpost.com/software/yoh-york-opportunity-hub','/static/media/yoh.37c063f1.jpg',1),(2,2,'Photo Gallery','Historical Sites of Toronto','This Web Application is an independent work of mine showcasing the photo collection of the Historical Sites of City of Toronto. This comes with an overlay on hover of images as well as a custom styled zoom modal view upon clicking on a photo.','https://github.com/nmuceros/FS1010_Assignment1_Style_Photo_Gallery','/static/media/photoGallery.48fe261f.png',1),(3,3,'Todo App','Interesting Drag-and-Drop Effect','This Todo Web Application is an independent work of mine. Unlike normal Todo application, this has an interesting drag and drop effect.','https://github.com/nmuceros/muceros__nolita--assignment-2','/static/media/todoApp.afeaf5f0.png',1),(4,4,'Creative Resonance','Mood Enhancing Site','This is a Web Application showcasing the Music Album photo collections from different genres. It has music which automatically plays while a selected genre is being viewed. It is a group effort from FS1000 Music Group. The Admin page is my major contribution to this project.','https://gitlab.com/nols2021/fs1000_summer2021_group1_project','/static/media/creativeResonance.eff4bdab.jpg',1);
/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-12  9:33:09
