# Project Name: Personal Portfolio Website (Database portion)

**Description**:  This is a React application which utilizes NodeJS for backend and MySQL for the database.

**Note**: This repo is the Database portion of my project. The frontend and backend repos are needed to be able to see the complete project package.


   ** Frontend Repo Link:** https://gitlab.com/nols2021/fs1030-nolita-muceros-portfolio-frontend

   ** Backend Repo Link:** https://gitlab.com/nols2021/fs1030-nolita-muceros-portfolio-backend


## Technologies
    
    The project has been developed using REACT, NodeJS and MySQL.

    Git Bash Terminal / Visual Studio Code / MySQL Workbench can be used to clone the project / work with the database.
    

**Project Repo Cloning Process**

    -Open bash terminal then create a folder where to store the repo

    -Change directory to the newly created folder

    -Clone the repo

        -At GitLab >> Go to Repository >> Select the 'main' repo from the dropdown
        -On the right side >> Right click on 'Clone' dropdown
        -Select Clone with HTTPS
        -Go back to the terminal then issue the command below (without the '<' and '>' symbols)
        
             git clone <paste the repo URL here>

    -Change directory to the repo folder "fs1030-nolita-muceros-portfolio-database"


## Diagrams

    Navigate to the 'diagrams' folder to see the following diagrams

        File Name       Description
        
        ifd-v2.pdf      IFD Diagram
        erd-v4-.pdf     ERD Diagram
        schema.pdf      Schema Diagram generated from MySQL Workbench


## Database Design Key Points

    (1) In order to create a Relational Database, I created Resume as parent table. 
        This contains my contact information and my objective. 
        The following are its child tables:
    
            Highlight - showcase my Highlights of Qualifications
        
            Highlight Detail - is the sub item of highlight above. I used this as I have to put sub items on one of my highlight.

            Education - holds my Education background
        
            Experience - showcase my past job experiences.
        
            Work Description - is the sub items of the Experience. It holds all the work responsibilities/accomplishments
                                I had from my previous job experiences


    (2) I have added the attribute called "Display Order" to the below tables for me to properly manage/maintain
        the proper ordering of the items/sub items and to make the ordering dynamic. This will save time in keeping
        the correct order of the items/sub items in case its NOT entered in a CHRONOLOGICAL ORDER by mistake.

            Highlight
            Highlight Detail
            Education
            Experience
            Work Description
            Portfolio
            


## mywebsite Database

   ** There are 2 ways to create my Database**

       Option #1: Import to MySQL Workbench

            -The exported EMR Database is stored in the 'exported-database' folder
            -Follow the steps below to import the database to MySQL Workbench

            **Steps To Import the EMR Database to MySQL Workbench**
            1. Open MySQL Workbench
            2. Go to Server menu >> Data Import
            3. Select Import from Self-Contained File then select the file to import
            4. Click the 'New' button in the Default Target Schema
            5. Enter 'mywebsite' on the Name of schema to create input field in the Create 
                Schema window
            6. Click the 'OK' button      
            7. Ensure the ‘Dump Structure and Data’ option is selected at the bottom
            8. Click the 'Start Import' button
            9. Refresh the screen

       Option #2: Run SQL Statements

        1. Navigate to'sqls' folder and find the 'portfolio.sql' file
        2. Copy and paste the SQL Statements inside it to the MySQL Workbench or Terminal then execute

        Note: Dataseeds are included in the exported database as well as in the portfolio.sql
