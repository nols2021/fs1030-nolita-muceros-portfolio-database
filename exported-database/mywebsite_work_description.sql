-- MySQL dump 10.13  Distrib 8.0.27, for macos11 (x86_64)
--
-- Host: localhost    Database: mywebsite
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `work_description`
--

DROP TABLE IF EXISTS `work_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `work_description` (
  `work_desc_id` int unsigned NOT NULL AUTO_INCREMENT,
  `display_order` int unsigned NOT NULL,
  `work_desc` varchar(255) DEFAULT NULL,
  `experience_id` int unsigned NOT NULL,
  PRIMARY KEY (`work_desc_id`,`display_order`),
  KEY `experience_id` (`experience_id`),
  CONSTRAINT `work_description_ibfk_1` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`experience_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_description`
--

LOCK TABLES `work_description` WRITE;
/*!40000 ALTER TABLE `work_description` DISABLE KEYS */;
INSERT INTO `work_description` VALUES (1,1,'Manages systems access provisioning and ensure compliance with Sorbanes-Oxley Act (SOX) as pertains to the company\'s internal controls',1),(2,2,'Maintains excellent channels of communication with all customers (internal clients) and internal IT departments',1),(3,3,'Ensured all Service Level Agreements are maintained and issues/requests are resolved as per this commitment',1),(4,1,'Automated internal manual reports using VBScript',2),(5,2,'Developed/executed automated test scripts using HP UFT',2),(6,3,'Planned and developed Test Plans and Test Cases to ensure development outputs meet defined specificatio cases in coordination with Business requirements, Technical Specifications, installer / upgrades documents, and management',2),(7,4,'Performed Functional / Regression / SIT / Smoke / UI / QA / Web Browser Compatibility testings and analyzed results',2),(8,1,'Automated Loan/lease Assumption Contract using VBScript',3),(9,2,'Automated internal manual reports using VBScript',3),(10,3,'Processed Retail and Lease contracts, rebooks and balloon refinanciang in accordance of company\'s policies and procedures ',3),(11,4,'Maintained strong dealer relationships by dealing with their inquiries promptly and professionally',3),(12,1,'Automated internal manual reports using VBScript',4),(13,2,'Maintained Invoice MS Access database',4),(14,3,'Maintained and controlled company’s Documents Change Request File (manually and electronically)',4),(15,4,'Processed invoices via internal system',4),(16,1,'Enhanced and maintained Sales and Accounts Receivable System per business requirements',5),(17,2,'Analyzed and resolved issues relating to business Systems and ensured internal control is met',5),(18,3,'Prepared and maintained System Training Manual and Functional Specifications Manuals',5),(19,4,'Technologies: FoxPro 2.6 / Visual FoxPro 6.0 / DOS / NOVELL 3.12',5),(20,1,'Enhanced and maintained customized Sales and Accounts Receivable System / General Ledger and Budget Monitoring System / Inventory Control',6),(21,2,'Converted existing programs from Clipper to FoxPro',6),(22,3,'Provided technical support to system users',6),(23,4,'Technologies: Clipper 5.2/ dBase / FoxPro 2.6 / Visual FoxPro 6.0 / DOS / NOVELL 3.12 / WINDOWS NT 4.0',6),(24,1,'Performed clerical duties and responsibilitites related to consignment processing',7);
/*!40000 ALTER TABLE `work_description` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-12  9:33:10
